<%-- 
    Document   : index
    Created on : 10-06-2021, 20:03:25
    Author     : Alex
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Endpoint Api Comunas</h1>
        
        <h3>Lista de Comunas (GET) - https://wfejemploapi.herokuapp.com/api/comunas/</h3>
        <h3>Lista de Comunas por id (GET) - https://wfejemploapi.herokuapp.com/api/comunas/{id}</h3>
        <h3>Crear comuna (POST) - https://wfejemploapi.herokuapp.com/api/comunas/</h3>
        <h3>Actualizar Comuna (PUT) - https://wfejemploapi.herokuapp.com/api/comunas/</h3>
        <h3>Eliminar comuna (DELETE) - https://wfejemploapi.herokuapp.com/api/comunas/{id}</h3>
    </body>
</html>
